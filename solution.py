#!/usr/bin/python
# -*- coding: utf-8 -*-
import pylab as pl
import numpy as np
from scipy.optimize import minimize
import scipy.io as sio

# n users, m factors

n = 3
m = 5

# load data

mat_contents = sio.loadmat('./data.mat')
extr = mat_contents['X']
A = extr[0:n, 0:m]


# object function

def f0(x):
    return np.log(1 + np.exp((-A * x).sum(axis=1))).sum()


# gradient

def f0prime(x):
    return ((-A).T / (1 + np.exp((A * x).sum(axis=1)))).sum(axis=1)


# def f0prime_help(x, i):
#     return (-A[i]) / (1 + np.exp((A[i] * x).sum()))

# def f0prime2(x):
#     return f0prime_help(x, 0) + f0prime_help(x, 1)

def f0hessian_help(x, i):
    return np.exp((A[i] * x).sum()) / (1 + np.exp((A[i] * x).sum())) \
        ** 2 * (A[0].reshape(m, 1) * A[i])


# Hessian (need vectorize)

def f0hessian(x):
    y = np.zeros(n)
    for i in range(n):
        y = y + f0hessian_help(x, i)
    return y


# equality constraint

cons = {'type': 'eq', 'fun': lambda x: x[0] + x[1] + x[2] + x[3] + x[4] - 1}

bnds = [(0, None), (0, None), (0, None), (0, None), (0, None)]

# strictly feasible point

x0 = np.array([.2, .2, .2, .2, .2])


# interior method

def phi(x):
    return -np.exp(x).sum()


def nf0(t):
    return lambda x: t * f0(x) + phi(x)


def nf0prime(t):
    return lambda x: t * f0prime(x) - 1 / x


iter_num = 20
res = np.zeros(iter_num)

for i in range(iter_num):
    opt = minimize(
        f0,
        x0,
        method='SLSQP',
        constraints=cons,
        jac=f0prime,
        bounds=bnds,
        options={'maxiter': i},
        )
    res[i] = opt.fun

x = np.arange(iter_num) + 1

pl.plot(x, res)

# pl.show()

pl.savefig('res.eps')
